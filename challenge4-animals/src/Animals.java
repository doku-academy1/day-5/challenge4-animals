public class Animals implements Identity {
    public String jenis, nama, makanan, gigi;

    public Animals(String jenis, String nama, String makanan, String gigi) {
        this.jenis = jenis;
        this.nama = nama;
        this.makanan = makanan;
        this.gigi = gigi;
    }

    public String getNama() {
        return nama;
    }

    public String getJenis() {
        return jenis;
    }

    public String getMakanan() {
        return makanan;
    }

    public String getGigi() {
        return gigi;
    }

    @Override
    public void identity_myself() {
        System.out.printf("Hi I'm %s, My name is %s", getJenis(), getNama());
    }
}
