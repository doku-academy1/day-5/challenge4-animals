public class Herbivor extends Animals {
    public Herbivor(String jenis, String nama, String makanan, String gigi) {
        super(jenis, nama, makanan, gigi);
    }

    @Override
    public void identity_myself() {
        super.identity_myself();
        System.out.printf(", My food is '%s', I have %s teeth", getMakanan(), getGigi());
    }
}
