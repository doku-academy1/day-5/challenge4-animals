import java.util.*;

public class Main {
    public static void main(String[] args) {
        String jenis, hewan;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Masukkan jenis hewan: ");
        jenis = keyboard.nextLine().toLowerCase();

        System.out.print("Masukkan nama hewan: ");
        hewan = keyboard.nextLine().toLowerCase();

        switch (jenis) {
            case "herbivor":
                Herbivor herbivor = new Herbivor(jenis, hewan, "tumbuhan", "tumpul");
                herbivor.identity_myself();
                break;
            case "karnivor":
                Carnivor carnivor = new Carnivor(jenis, hewan, "daging", "tajam");
                carnivor.identity_myself();
                break;
            case "omnivor":
                Omnivor omnivor = new Omnivor(jenis, hewan, "semua", "tajam dan tumpul");
                omnivor.identity_myself();
                break;
            default:
                System.out.println("Jenis hewan tidak sesuai!");
        }
    }
}